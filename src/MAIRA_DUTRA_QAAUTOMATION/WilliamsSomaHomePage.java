package MAIRA_DUTRA_QAAUTOMATION;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.apache.commons.io.FileUtils;


public class WilliamsSomaHomePage {

	private WebDriver driver;
		
	
	public  WilliamsSomaHomePage(WebDriver driver){
		this.driver = driver;
	}

	public void homePageStart(){
		driver.get("http://www.williams-sonoma.com/");
	}

	public void maximize(){
		driver.manage().window().maximize();
	}
	
	public boolean existe(String string){
		return driver.getPageSource().contains(string);
	}
	
	public void openMenu(String menu, String submenu){
		Actions actions = new Actions(driver);
        WebElement menuDropDown = driver.findElement(By.linkText(menu));
        actions.moveToElement(menuDropDown);
        //actions.click();
        actions.perform();
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        driver.findElement(By.linkText(submenu)).click();
	}
	
	public void chooseProduct(String product, String quant){
		driver.findElement(By.linkText(product)).click();
		driver.findElement(By.xpath("//div[@id='pip']/div/div[6]/div[2]/div[2]/section/section/div/div/div/div[2]/div/input")).sendKeys(quant);
	}
	
	public void addcart(){
		driver.findElement(By.xpath("//div[@id='pip']/div/div[6]/div[2]/div[2]/section/div/div/fieldset/button")).click();
	}
	
	public void checkout() throws IOException{
		        
		WebElement modalPanel = driver.findElement(By.id("racOverlay"));
		modalPanel.findElement(By.xpath("//div[@id='btn-checkout']/a")).click();
	}

	public void takescreen(String path) throws IOException{
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(path));
	}

	public void waiting(){
		driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);  
	}
	
    public void search(String product){
    	driver.findElement(By.id("search-field")).sendKeys(product);
    	driver.findElement(By.id("btnSearch")).click();
    	
    }
    public void quicklook(){
    	Actions actions = new Actions(driver);
        WebElement productlook = driver.findElement(By.linkText("Le Creuset Heritage Cast-Iron Fry Pan"));
        actions.moveToElement(productlook);
        //actions.click();
        actions.perform();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//div[@id='content']/div[2]/ul/li[3]/div/a[2]/span")).click();
    }

    
	
}
