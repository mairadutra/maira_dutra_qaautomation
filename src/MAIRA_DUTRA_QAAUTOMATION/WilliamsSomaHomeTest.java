package MAIRA_DUTRA_QAAUTOMATION;
import static org.junit.Assert.assertTrue;
 



import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;




public class WilliamsSomaHomeTest {
	
	private WebDriver driver;
	private WilliamsSomaHomePage williams;
	
	@Before
	public void inicializa(){
		this.driver = new FirefoxDriver();
		williams = new WilliamsSomaHomePage(driver);
	}
	
	@Test
	public void buyProduct() throws InterruptedException, IOException{
		
		williams.homePageStart();
		williams.maximize();
		assertTrue(williams.existe("Cookware"));
		
		williams.openMenu("Cookware", "Cookware Sets");
		assertTrue(williams.existe("Cookware Sets"));
		
		williams.chooseProduct("All-Clad d5 Stainless-Steel 15-Piece Cookware Set", "1");
		williams.waiting();
		assertTrue(williams.existe("Special Offer"));
		
		williams.addcart();
		williams.waiting();
		assertTrue(williams.existe("You've just added the following to your basket:"));
		String path = "c:\\teste\\cart.png";
		williams.takescreen(path);
		
		williams.checkout();
		assertTrue(williams.existe("Shopping Cart"));
		
		williams.search("fry pan");
		williams.waiting();
		assertTrue(williams.existe("Search Results for:"));
		williams.quicklook();
		williams.waiting();
		String path2 = "c:\\teste\\quick.png";
		williams.takescreen(path2);
	}
	
	@After
	public void finaliza(){
		driver.quit();
	}
}
